var xlsx = require('xlsx')
const https = require("https");
const fs = require('fs');
const pdf = require('pdf-parse');

var wb = xlsx.readFile('Kriyadocs_WIP_WDNI.xlsx')

const getPdfData = (pdf_url, unique_id, articleStage, articleStatus, customer_project) => {
    const url =pdf_url;
    var request = https.get(url, (res) => {
        const path = '../pdfs10/'+ unique_id + '.pdf'
        const writeStream = fs.createWriteStream(path);
        res.pipe(writeStream);
        writeStream.on("finish", () => {
            getPageCount(path, unique_id, articleStage, articleStatus, customer_project)
            writeStream.close();
        })  
    })
    request.on('error', function (e) {
        console.log('0')
    });
    
    request.end();
}

const getPageUrl = async () => {
    let n = wb.SheetNames.length
    let wsName = ''
    for (let i=0; i<n; i++ ) {
        wsName = wb.SheetNames[i]
        const workSheet = wb.Sheets[wsName]
        const data = xlsx.utils.sheet_to_json(workSheet)
        for (let val of data) {
            if (val['Page Count'] === 0) {
                let output = wsName.split('_')
                let customer = output[0]
                let project = output[1]
                let articleId = val['Article Number']
                let articleStage = val['Article Stage']
                let articleStatus = val['Article Status']
                let customer_project = customer + '_'+ project
                let pdf_url = 'https://resources.kriyadocs.com/resources/'+ customer.toLocaleLowerCase()+'/'+ project.toLocaleLowerCase() + '/'+ articleId + '/proofing/'+ articleId +'.pdf'
                await getPdfData(pdf_url, articleId, articleStage, articleStatus, customer_project) 
            }
        }
    }    
    
}

const convertJsonToExcel = (sheet_Data) => {
    const workSheet = xlsx.utils.json_to_sheet(sheet_Data);
    const workBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(workBook, workSheet, "sheet_Data")
    xlsx.write(workBook, { bookType: 'xlsx', type: "buffer" })
    xlsx.write(workBook, { bookType: "xlsx", type: "binary" })
    xlsx.writeFile(workBook, "New_Shreadsheet.xlsx")
    console.log('created')
}

let myArray = []
let pair = {}
const getPageCount =  (url, articleId, articleStage, articleStatus, customer_project) => {
    let dataBuffer = fs.readFileSync(url);
    pdf(dataBuffer).then(function(data) {
    let pageCount = data.numpages
    pair = {
        'Article Number': articleId,
        'Article Stage': articleStage,
        'Article Status': articleStatus,
        'Page count': pageCount,
        'Customer & Project Name': customer_project
    }
    myArray.push(pair)
    convertJsonToExcel(myArray)
        
    })
    .catch(function(error){
        pair = {
            'Article Number': articleId,
            'Article Stage': articleStage,
            'Article Status': articleStatus,
            'Page count': ' ',
            'Customer & Project Name': customer_project,
            'Reason': 'file read error in AWS'
            
        }
        myArray.push(pair)
        convertJsonToExcel(myArray)
    })
}  

getPageUrl()