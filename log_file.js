const https = require('https');
var fs = require('fs');
var xlsx = require('xlsx')

var wb = xlsx.readFile('Kriyadocs_WIP_WDNI.xlsx')

let file_name = 'log_file3.txt'
const writeFile = (file_name, myData) => {
    fs.writeFileSync(file_name, myData);
}

let myData = ''
let myLog = []
const getPdfData = (url, DOI) => {
    https.get(url, res =>  {
        let headerDate = res.headers.date
        let timeStamp = new Date(headerDate).toLocaleString('en-GB', {timeZone: 'Asia/Kolkata'});
        let statusCode = res.statusCode
        let failure_reason = ''
        
        if (statusCode !== 200) {
            failure_reason = 'ERROR : file read error in AWS'
        }
        let obj = {
            'Timestamp of the request': timeStamp,
            'Status Code': statusCode,
            'Constructed url': url,
            'DOI': DOI,
            'Failure Reason': failure_reason
        }
        myData = JSON.stringify(obj);
        myLog.push(myData+'\n')
        
        writeFile(file_name, myLog.toString())
    }).on('error', err => {
    console.log('Error: ', err.message);
    });
}

const getPageUrl = () => {
    let n = wb.SheetNames.length
    let wsName = ''
    for (let i=0; i<n; i++ ) {
        wsName = wb.SheetNames[i]
        const workSheet = wb.Sheets[wsName]
        const data = xlsx.utils.sheet_to_json(workSheet)
        for (let val of data) {
            if (val['Page Count'] === 0) {
                let output = wsName.split('_')
                let customer = output[0]
                let project = output[1]
                 DOI = val['DOI']
                let articleId = val['Article Number']
                 pdf_url = 'https://resources.kriyadocs.com/resources/'+ customer.toLocaleLowerCase()+'/'+ project.toLocaleLowerCase() + '/'+ articleId + '/proofing/'+ articleId +'.pdf'
                getPdfData(pdf_url, DOI)  
            }
        }
        
    }     
}

getPageUrl()